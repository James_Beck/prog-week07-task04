﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace VoidMethodExportDoubleCalculation
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 1;

            do
            {
                Console.WriteLine("Input two numbers for us?");
                try
                {
                    calculation(double.Parse(Console.ReadLine()), double.Parse(Console.ReadLine()));
                }
                catch
                {
                    Console.WriteLine("Yeah so those one or both of those inputs weren't adequate numbers");
                    Thread.Sleep(2000);
                    Console.Clear();
                    i = 0;
                }
            } while (i == 0);
        }

        static void calculation(double a, double b)
        {
            Console.WriteLine("Did you know, {0} + {1} is equal to {2}?", a, b, a + b);
            Console.WriteLine("Did you know, {0} - {1} is equal to {2}?", a, b, a - b);
            Console.WriteLine("Did you know, {0} * {1} is equal to {2}?", a, b, a * b);
            Console.WriteLine("Did you know, {0} / {1} is equal to {2}?", a, b, a / b);
        }
    }
}
